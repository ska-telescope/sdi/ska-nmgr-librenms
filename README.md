# ska-nmgr-librenms

This project provides a Helm Chart for deploying LibreNMS .

[![Documentation Status](https://readthedocs.org/projects/ska-nmgr-librenms/badge/?version=latest)](https://developer.skatelescope.org/projects/ska-nmgr-librenms/en/latest/?badge=latest)

This project provides a Helm Chart for [LibreNMS](https://www.librenms.org/).

## Installation

Configuration is based on the [values.yaml](https://gitlab.com/ska-telescope/sdi/ska-nmgr-librenms/-/blob/main/charts/ska-nmgr-librenms/values.yaml?ref_type=heads).  Look at this to see what configuration options there are.

Checkout the repository and install using:
```
$ make k8s-install-chart
```

## Configuration

Once the chart is installed and the librenms application suite has come up, it then needs to be configured.
Start by finishing the install and creating the `admin` account by going to the installation URL: `http://<IP or DNS>/librenms/install`.
Once completed, log back in again and then validate the install by going to the validate URL:  `http://<IP or DNS>/librenms/validate`.
This will give a list of things that may need fixing, such as the polling functions - https://docs.librenms.org/Extensions/Dispatcher-Service/#distributed-polling-configuration .

## Need to create an admin user?

See: https://community.librenms.org/t/how-to-reinstall/13553/3
Shell into librenms Pod:
```
lnms user:add --role=admin admin
```
